﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Lab5
{
    public partial class Form1 : Form
    {
        private Class.Music _Music;
        public Class.Music Music
        {
            get { return _Music; }
            set { _Music = value; }
        }
        private Form2 _addForm;
        public Form2 AddForm
        {
            get { return _addForm; }
            set { _addForm = value; }
        }
        Class.Music CreateAlbum()
        {
            Class.Classic Classic = new Class.Classic();
            Classic.Name = "'Лунная соната'";
            Classic.On_a_plate = 1.05;
            Classic.Duration = 394 * Classic.On_a_plate;

            Class.Pop Pop = new Class.Pop();
            Pop.Name = "'Thunder'";
            Pop.Radio_version = 0.5;
            Pop.Duration = 187 * Pop.Radio_version;

            Class.Music Music = new Class.Music();
            Music.Album.Add(Classic);
            Music.Album.Add(Pop);

            return Music;
        }
        void InitializeListBox()
        {
            for (int i = 0; i < Music.Album.Count; i++)
            {
                listBox1.Items.Add(Music.Album[i].Name + " - " + Music.Album[i].Duration.ToString());
            }
        }
        void calculator()
        {
            Class.Duration calculator = new Class.Duration();
            label1.Text = "Продолжительность альбома:" + calculator.Get_total_duration(Music).ToString();
        }
        void AddClass(Class.Abstract_composition Class)
        {
            Music.Album.Add(Class);
            listBox1.Items.Add(Class.Name + " - " + Music.Album[Music.Album.Count - 1].Duration);
        }
        void EditClass(Class.Abstract_composition Class)
        {
            int Selected = listBox1.SelectedIndex;
            listBox1.Items.RemoveAt(Selected);
            listBox1.Items.Insert(Selected, Class.Name + " - " + Class.Duration.ToString());
            Music.Album[Selected] = Class;
        }
        void DeleteClass()
        {
            if (listBox1.SelectedItem != null)
            {
                Music.Album.RemoveAt(listBox1.SelectedIndex);
                listBox1.Items.Remove(listBox1.SelectedItem);
            }
        }
        public void CheckResult()
        {
            if (AddForm.Result == DialogResult.OK)
            {
                if (!AddForm.Edited)
                    AddClass(AddForm.Music);
                else
                    EditClass(AddForm.Music);
                calculator();
            }
        }
        public Form1()
        {
            Music = CreateAlbum();
            InitializeComponent();
            InitializeListBox();
            calculator();
        }
        private void AddButton_Click(object sender, EventArgs e)
        {
            AddForm = new Form2();
            AddForm.Owner = this;
            AddForm.ShowDialog();
        }
        private void DeleteButton_Click(object sender, EventArgs e)
        {
            DeleteClass();
            calculator();
        }
        private void EditButton_Click(object sender, EventArgs e)
        {
            if (listBox1.SelectedItem != null)
            {
                AddForm = new Form2(Music.Album[listBox1.SelectedIndex]);
                AddForm.Owner = this;
                AddForm.ShowDialog();
            }
        }
    }
}
