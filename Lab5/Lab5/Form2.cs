﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Lab5
{
    public partial class Form2 : Form
    {
        private DialogResult _result;
        public DialogResult Result
        {
            get { return _result; }
            set { _result = value; }
        }
        private bool _edited;
        public bool Edited
        {
            get { return _edited; }
            set { _edited = value; }
        }
        private Class.Abstract_composition _music;
        public Class.Abstract_composition Music
        {
            get { return _music; }
            set { _music = value; }
        }

        string SelectMusic(Class.Abstract_composition Music)
        {
            string Type = "";
            if (Music is Class.Folk)
                Type = "Народная";
            if (Music is Class.Classic)
                Type = "Классическая";
            if (Music is Class.Pop)
                Type = "Поп-музыка";
            if (Music is Class.Spiritual)
                Type = "Духовная";
            return Type;
        }
        void CreateMusic()
        {
            switch (classBox.Text)
            {
                case "Народная":
                    Class.Folk Folk = new Class.Folk();
                    Folk.On_the_parade = double.Parse(SpecialTextBox.Text);
                    Folk.Duration = double.Parse(durationBox.Text) * Folk.On_the_parade;
                    Music = Folk;
                    break;
                case "Классическая":
                    Class.Classic Classic = new Class.Classic();
                    Classic.On_a_plate = double.Parse(SpecialTextBox.Text);
                    Classic.Duration = double.Parse(durationBox.Text) * Classic.On_a_plate;
                    Music = Classic;
                    break;
                case "Поп-музыка":
                    Class.Pop Pop = new Class.Pop();
                    Pop.Radio_version = double.Parse(SpecialTextBox.Text);
                    Pop.Duration = double.Parse(durationBox.Text) * Pop.Radio_version;
                    Music = Pop;
                    break;
                case "Духовная":
                    Class.Spiritual Spiritual = new Class.Spiritual();
                    Spiritual.On_holiday = double.Parse(SpecialTextBox.Text);
                    Spiritual.Duration = double.Parse(durationBox.Text) * Spiritual.On_holiday;
                    Music = Spiritual;
                    break;
            }
            Music.Name = nameBox.Text;
        }
        public Form2()
        {
            Edited = false;
            InitializeComponent();
        }
        public Form2(Class.Abstract_composition _music)
        {
            Edited = true;
            InitializeComponent();
            Music = _music;
            if (Music != null)
            {
                nameBox.Text = Music.Name;
                durationBox.Text = Music.Duration.ToString();

                if (_music is Class.Folk && (_music as Class.Folk).On_the_parade!=1)
                {
                    SpecialTextBox.Text = (_music as Class.Folk).On_the_parade.ToString();
                    SpecialCheckBox.Checked = true;
                }
                else if (_music is Class.Classic && (_music as Class.Classic).On_a_plate != 1)
                {
                    SpecialTextBox.Text = (_music as Class.Classic).On_a_plate.ToString();
                    SpecialCheckBox.Checked = true;
                }
                else if (_music is Class.Pop && (_music as Class.Pop).Radio_version != 1)
                {
                    SpecialTextBox.Text = (_music as Class.Pop).Radio_version.ToString();
                    SpecialCheckBox.Checked = true;
                }
                else if (_music is Class.Spiritual && (_music as Class.Spiritual).On_holiday != 1)
                {
                    SpecialTextBox.Text = (_music as Class.Spiritual).On_holiday.ToString();
                    SpecialCheckBox.Checked = true;
                }
                else
                {
                    SpecialCheckBox.Checked = false;
                }

                classBox.Text = SelectMusic(Music);
            }
        }
       
        private void OK_Click_1(object sender, EventArgs e)
        {
            Result = DialogResult.OK;
            CreateMusic();
            Form1 main = Owner as Form1;
            main.CheckResult();
            this.Close();
        }
        private void Cancel_Click(object sender, EventArgs e)
        {
            Result = DialogResult.Cancel;
            this.Close();
        }
        private void classBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (classBox.SelectedIndex != -1) SpecialCheckBox.Visible = true;
            switch (classBox.Text)
            {
                case "Народная":
                    Class.Folk Folk = new Class.Folk();
                    Folk.Duration = double.Parse(durationBox.Text);
                    Folk.On_the_parade = double.Parse(SpecialTextBox.Text);
                    SpecialCheckBox.Text = "На параде";
                    Music = Folk;
                    break;
                case "Классическая":
                    SpecialCheckBox.Text = "На пластинке";
                    break;
                case "Поп-музыка":
                    SpecialCheckBox.Text = "Радио-версия";
                    break;
                case "Духовная":
                    SpecialCheckBox.Text = "На празднике";
                    break;
            }
        }
        private void Box_KeyPress(object sender, KeyPressEventArgs e)
        {
            if ((e.KeyChar <= 47 || e.KeyChar >= 58) && e.KeyChar != 8 && e.KeyChar != 44)
                e.Handled = true;
        }
        private void SpecialCheckBox_CheckedChanged(object sender, EventArgs e)
        {
            if (SpecialCheckBox.Checked) SpecialTextBox.Visible = true;
            else SpecialTextBox.Visible = false;
        }

        private void classBox_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = true;
        }
    }
}
