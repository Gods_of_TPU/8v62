﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Class
{
    public class Music
    {
        private List<Abstract_composition> _album= new List<Abstract_composition>();
        public List<Abstract_composition> Album
        {
            get { return _album; }
            set { _album = value; }
        }
    }
    public abstract class Abstract_composition
    {
        protected double _duration;
        public double Duration
        {
            get { return _duration; }
            set { _duration = value; }
        }
        string _name;
        public string Name
        {
            get { return _name; }
            set { _name = value; }
        }
    }
    public class Folk : Abstract_composition
    {
        private double _on_the_parade;
        public double On_the_parade
        {
            get { return _on_the_parade; }
            set { _on_the_parade = value; }
        }
    }
    public class Classic : Abstract_composition
    {
        private double _on_a_plate;
        private double _remix;
        public double On_a_plate
        {
            get { return _on_a_plate; }
            set { _on_a_plate = value; }
        }
         public double Remix
        {
            get { return _remix; }
            set { _remix = value; }
        }
    }
    public class Pop : Abstract_composition
    {
        private double radio_version;
        public double Radio_version
        {
            get { return radio_version; }
            set { radio_version = value; }
        }
    }
    public class Spiritual : Abstract_composition
    {
        private double _on_holiday;
        public double On_holiday
        {
            get { return _on_holiday; }
            set { _on_holiday = value; }
        }
    }
}
