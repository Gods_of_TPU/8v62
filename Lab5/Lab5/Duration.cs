﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Class
{
    public class Duration
    {
        public double Get_total_duration(Class.Music Music)
        {
            double SumDuration = 0;
            for (int i=0; i< Music.Album.Count; i++)
            {
                SumDuration += Music.Album[i].Duration;
            }
            return SumDuration;
        }
    }
}